//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System.Linq;
using Tecan.Sila2;
using Tecan.Sila2.Client;
using Tecan.Sila2.Server;

namespace Tecan.Sila2.Generator.Test.TestReference
{
    
    
    ///  <summary>
    /// Class that implements the IDictionaryService interface through SiLA2
    /// </summary>
    public partial class DictionaryServiceClient : IDictionaryService
    {
        
        private System.Lazy<System.Collections.Generic.IDictionary<long, string>> _lookup;
        
        private IClientExecutionManager _executionManager;
        
        private IClientChannel _channel;
        
        private const string _serviceName = "sila2.tecan.sila.generator.test.dictionaryservice.v4.DictionaryService";
        
        ///  <summary>
        /// Creates a new instance
        /// </summary>
        /// <param name="channel">The channel through which calls should be executed</param>
        /// <param name="executionManager">A component to determine metadata to attach to any requests</param>
        public DictionaryServiceClient(IClientChannel channel, IClientExecutionManager executionManager)
        {
            _executionManager = executionManager;
            _channel = channel;
            InitLazyRequests();
        }
        
        ///  <summary>
        /// </summary>
        public virtual System.Collections.Generic.IDictionary<long, string> Lookup
        {
            get
            {
                return _lookup.Value;
            }
        }
        
        private System.Collections.Generic.IDictionary<long, string> RequestLookup()
        {
            Tecan.Sila2.Client.IClientCallInfo callInfo = _executionManager.CreateCallOptions("tecan.sila.generator/test/DictionaryService/v4/Property/Lookup");
            try
            {
                System.Collections.Generic.IDictionary<long, string> response = Tecan.Sila2.DtoExtensions.Extract(_channel.ReadProperty<PropertyResponse<System.Collections.Generic.List<Tecan.Sila2.KeyValuePairDto<long, Tecan.Sila2.IntegerDto, string, Tecan.Sila2.StringDto>>>>(_serviceName, "Lookup", "tecan.sila.generator/test/DictionaryService/v4/Property/Lookup", callInfo).Value, _executionManager.DownloadBinaryStore);
                callInfo.FinishSuccessful();
                return response;
            } catch (System.Exception ex)
            {
                System.Exception exception = _channel.ConvertException(ex);
                callInfo.FinishWithErrors(exception);
                throw exception;
            }
        }
        
        ///  <summary>
        /// Initializes lazies for non-observable properties.
        /// </summary>
        private void InitLazyRequests()
        {
            _lookup = new System.Lazy<System.Collections.Generic.IDictionary<long, string>>(RequestLookup);
        }
        
        private T Extract<T>(Tecan.Sila2.ISilaTransferObject<T> dto)
        
        {
            return dto.Extract(_executionManager.DownloadBinaryStore);
        }
    }
    
    ///  <summary>
    /// Factory to instantiate clients for the Dictionary Service.
    /// </summary>
    [System.ComponentModel.Composition.ExportAttribute(typeof(IClientFactory))]
    [System.ComponentModel.Composition.PartCreationPolicyAttribute(System.ComponentModel.Composition.CreationPolicy.Shared)]
    public partial class DictionaryServiceClientFactory : IClientFactory
    {
        
        ///  <summary>
        /// Gets the fully-qualified identifier of the feature for which clients can be generated
        /// </summary>
        public string FeatureIdentifier
        {
            get
            {
                return "tecan.sila.generator/test/DictionaryService/v4";
            }
        }
        
        ///  <summary>
        /// Gets the interface type for which clients can be generated
        /// </summary>
        public System.Type InterfaceType
        {
            get
            {
                return typeof(IDictionaryService);
            }
        }
        
        ///  <summary>
        /// Creates a strongly typed client for the given execution channel and execution manager
        /// </summary>
        /// <param name="channel">The channel that should be used for communication with the server</param>
        /// <param name="executionManager">The execution manager to manage metadata</param>
        /// <returns>A strongly typed client. This object will be an instance of the InterfaceType property</returns>
        public object CreateClient(IClientChannel channel, IClientExecutionManager executionManager)
        {
            return new DictionaryServiceClient(channel, executionManager);
        }
    }
}


﻿using System.Reflection;

[assembly: AssemblyTitle( "Tecan.Sila2.InteropServer.NetCore" )]
[assembly: AssemblyDescription( "An interoperability server implementation based on ASP.NET Core" )]
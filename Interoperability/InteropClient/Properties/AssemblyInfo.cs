﻿using System.Reflection;

[assembly: AssemblyTitle( "Tecan.Sila2.InteropClient" )]
[assembly: AssemblyDescription( "Interoperability client implementation using unmanaged gRPC" )]
﻿using Tecan.Sila2.Server;

namespace Tecan.Sila2.Interop.Server
{
    public partial interface IComplexDataTypeTest
    {
        IRequestInterceptor User
        {
            get;
        }
    }
}

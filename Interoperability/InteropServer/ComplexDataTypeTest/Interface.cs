//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Tecan.Sila2.Interop.Server
{
    
    
    ///  <summary>
    /// Response type for the Why make it simple when you can make it complicated command
    /// </summary>
    [Tecan.Sila2.SilaDisplayNameAttribute("Why make it simple when you can make it complicated - Response")]
    public struct WhyMakeItSimpleWhenYouCanMakeItComplicatedResponse
    {
        
        private string _firstResponse;
        
        private string _secondResponse;
        
        ///  <summary>
        /// Initializes a new instance
        /// </summary>
        public WhyMakeItSimpleWhenYouCanMakeItComplicatedResponse(string firstResponse, string secondResponse)
        {
            _firstResponse = firstResponse;
            _secondResponse = secondResponse;
        }
        
        ///  <summary>
        /// The first response
        /// </summary>
        public string FirstResponse
        {
            get
            {
                return _firstResponse;
            }
        }
        
        ///  <summary>
        /// The second response
        /// </summary>
        public string SecondResponse
        {
            get
            {
                return _secondResponse;
            }
        }
    }
    
    ///  <summary>
    /// This is a test feature, which can be used to test various SiLA data types.
    /// It provides complex commands and properties.
    /// </summary>
    [Tecan.Sila2.SilaFeatureAttribute(true, "test")]
    [Tecan.Sila2.SilaIdentifierAttribute("ComplexDataTypeTest")]
    public partial interface IComplexDataTypeTest
    {
        
        ///  <summary>
        /// A list containing a lot of elements
        /// </summary>
        [Tecan.Sila2.SilaDisplayNameAttribute("A Huge List")]
        System.Collections.Generic.ICollection<string> HugeList
        {
            get;
        }
        
        ///  <summary>
        /// Displays a random boolean property
        /// </summary>
        [Tecan.Sila2.SilaDisplayNameAttribute("Boolean property")]
        bool Boolean
        {
            get;
        }
        
        ///  <summary>
        /// A list of string
        /// </summary>
        [Tecan.Sila2.SilaDisplayNameAttribute("A list of string")]
        System.Collections.Generic.ICollection<string> ListString
        {
            get;
        }
        
        ///  <summary>
        /// A randomly changing list of strings
        /// </summary>
        [Tecan.Sila2.SilaDisplayNameAttribute("A randomly changing list of strings")]
        System.Collections.Generic.ICollection<string> RandomChangingListString
        {
            get;
        }
        
        ///  <summary>
        /// A list of structure with a pair of string
        /// </summary>
        [Tecan.Sila2.SilaDisplayNameAttribute("A list of structure with a pair of string")]
        System.Collections.Generic.IDictionary<string, string> ListStructPairString
        {
            get;
        }
        
        ///  <summary>
        /// A structure with a pair of string
        /// </summary>
        [Tecan.Sila2.SilaDisplayNameAttribute("A structure with a pair of string")]
        StructPairString StructPairString
        {
            get;
        }
        
        ///  <summary>
        /// Return a datatype
        /// </summary>
        [Tecan.Sila2.SilaDisplayNameAttribute("A Datatype")]
        DataTypeString DataType
        {
            get;
        }
        
        ///  <summary>
        /// Throw a test exception when trying to retrieve the value of the property
        /// </summary>
        [Tecan.Sila2.SilaDisplayNameAttribute("Throw a test exception")]
        string ThrowException
        {
            get;
        }
        
        ///  <summary>
        /// Provide a list
        /// </summary>
        /// <param name="listLength">Number of elements in the list</param>
        /// <param name="elementLength">Length of each elements</param>
        /// <returns>A list of string</returns>
        [return: Tecan.Sila2.SilaIdentifierAttribute("List")]
        System.Collections.Generic.ICollection<string> ListProvider(long listLength, long elementLength);
        
        ///  <summary>
        /// Three dimensional structure
        /// </summary>
        /// <param name="structStructStruct">Structure of structure of structure</param>
        /// <returns>³d list</returns>
        [Tecan.Sila2.SilaDisplayNameAttribute("Three dimensional structure")]
        [return: Tecan.Sila2.SilaIdentifierAttribute("ThreeDimensionalListResult")]
        ThreeDimensionalString ThreeDimensionalStruct([Tecan.Sila2.SilaDisplayNameAttribute("Structure³")] ThreeDimensionalStructStructStructStruct structStructStruct);
        
        ///  <summary>
        /// Three dimensional list
        /// </summary>
        /// <param name="listListList">List of list of list</param>
        /// <returns>³d structure</returns>
        [Tecan.Sila2.SilaDisplayNameAttribute("Three dimensional list")]
        [return: Tecan.Sila2.SilaIdentifierAttribute("ThreeDimensionalStructResult")]
        [return: Tecan.Sila2.SilaDisplayNameAttribute("Three Dimensional Structure Result")]
        ThreeDimensionalListThreeDimensionalStructResult ThreeDimensionalList([Tecan.Sila2.SilaDisplayNameAttribute("List³")] ThreeDimensionalString listListList);
        
        ///  <summary>
        /// Really complicated
        /// </summary>
        /// <param name="structListStruct">A structure of list of structure</param>
        /// <param name="listStructList">A list of structure of list</param>
        /// <returns>The first response, The second response</returns>
        [Tecan.Sila2.SilaDisplayNameAttribute("Why make it simple when you can make it complicated")]
        [return: Tecan.Sila2.InlineStructAttribute()]
        WhyMakeItSimpleWhenYouCanMakeItComplicatedResponse WhyMakeItSimpleWhenYouCanMakeItComplicated([Tecan.Sila2.SilaDisplayNameAttribute("Structure List Structure")] WhyMakeItSimpleWhenYouCanMakeItComplicatedStructListStruct structListStruct, [Tecan.Sila2.SilaDisplayNameAttribute("List Structure List")] System.Collections.Generic.ICollection<WhyMakeItSimpleWhenYouCanMakeItComplicatedListStructListItem> listStructList);
        
        ///  <summary>
        /// Encipher binary data using XOR Cipher
        /// </summary>
        /// <param name="data">The data to encipher</param>
        /// <param name="cipherKey">The key to encipher the data with</param>
        /// <returns>The enciphered data</returns>
        [Tecan.Sila2.SilaDisplayNameAttribute("XOREncipher")]
        [return: Tecan.Sila2.SilaIdentifierAttribute("Data")]
        System.IO.Stream XOREncipher(System.IO.Stream data, [Tecan.Sila2.MaximalInclusiveAttribute(255D)] [Tecan.Sila2.MinimalInclusiveAttribute(0D)] [Tecan.Sila2.SilaDisplayNameAttribute("CipherKey")] long cipherKey);
        
        ///  <summary>
        /// Provides a value for the specified type
        /// </summary>
        /// <param name="type">The wanted type</param>
        /// <returns>Any</returns>
        [return: Tecan.Sila2.SilaIdentifierAttribute("Any")]
        Tecan.Sila2.DynamicClient.DynamicObjectProperty ValueForTypeProvider(string type);
    }
    
    ///  <summary>
    /// A one dimensional list of string
    /// </summary>
    [Tecan.Sila2.SilaDisplayNameAttribute("One dimensional List of string")]
    public struct OneDimensionalString
    {
        
        private System.Collections.Generic.ICollection<string> _value;
        
        ///  <summary>
        /// Initializes a new instance
        /// </summary>
        public OneDimensionalString(System.Collections.Generic.ICollection<string> value)
        {
            _value = value;
        }
        
        ///  <summary>
        /// The inner value
        /// </summary>
        public System.Collections.Generic.ICollection<string> Value
        {
            get
            {
                return _value;
            }
        }
    }
    
    ///  <summary>
    /// Used as a response by the ValueForTypeProvider
    /// </summary>
    public struct AnonymousList
    {
        
        private System.Collections.Generic.ICollection<Tecan.Sila2.DynamicClient.DynamicObjectProperty> _value;
        
        ///  <summary>
        /// Initializes a new instance
        /// </summary>
        public AnonymousList(System.Collections.Generic.ICollection<Tecan.Sila2.DynamicClient.DynamicObjectProperty> value)
        {
            _value = value;
        }
        
        ///  <summary>
        /// The inner value
        /// </summary>
        public System.Collections.Generic.ICollection<Tecan.Sila2.DynamicClient.DynamicObjectProperty> Value
        {
            get
            {
                return _value;
            }
        }
    }
    
    ///  <summary>
    /// A Two dimensional list of string
    /// </summary>
    [Tecan.Sila2.SilaDisplayNameAttribute("Two dimensional List of string")]
    public struct TwoDimensionalString
    {
        
        private System.Collections.Generic.ICollection<OneDimensionalString> _value;
        
        ///  <summary>
        /// Initializes a new instance
        /// </summary>
        public TwoDimensionalString(System.Collections.Generic.ICollection<OneDimensionalString> value)
        {
            _value = value;
        }
        
        ///  <summary>
        /// The inner value
        /// </summary>
        public System.Collections.Generic.ICollection<OneDimensionalString> Value
        {
            get
            {
                return _value;
            }
        }
    }
    
    ///  <summary>
    /// A Three dimensional list of string
    /// </summary>
    [Tecan.Sila2.SilaDisplayNameAttribute("Three dimensional List of string")]
    public struct ThreeDimensionalString
    {
        
        private System.Collections.Generic.ICollection<TwoDimensionalString> _value;
        
        ///  <summary>
        /// Initializes a new instance
        /// </summary>
        public ThreeDimensionalString(System.Collections.Generic.ICollection<TwoDimensionalString> value)
        {
            _value = value;
        }
        
        ///  <summary>
        /// The inner value
        /// </summary>
        public System.Collections.Generic.ICollection<TwoDimensionalString> Value
        {
            get
            {
                return _value;
            }
        }
    }
    
    ///  <summary>
    /// A data type representing a String
    /// </summary>
    [Tecan.Sila2.SilaDisplayNameAttribute("A Data Type string")]
    public struct DataTypeString
    {
        
        private string _value;
        
        ///  <summary>
        /// Initializes a new instance
        /// </summary>
        public DataTypeString(string value)
        {
            _value = value;
        }
        
        ///  <summary>
        /// The inner value
        /// </summary>
        public string Value
        {
            get
            {
                return _value;
            }
        }
    }
    
    ///  <summary>
    /// Sorry but there is no more sugar !
    /// </summary>
    public class SugarExceptionException : System.Exception
    {
        
        ///  <summary>
        /// Create a new instance
        /// </summary>
        /// <param name="message">The actual error message</param>
        public SugarExceptionException(string message) : 
                base(message)
        {
        }
    }
    
    ///  <summary>
    /// You just encountered a test exception !
    /// </summary>
    public class TestExceptionException : System.Exception
    {
        
        ///  <summary>
        /// Create a new instance
        /// </summary>
        /// <param name="message">The actual error message</param>
        public TestExceptionException(string message) : 
                base(message)
        {
        }
    }
    
    ///  <summary>
    /// The class User reflects an anonymous type from the feature definition.
    /// </summary>
    public struct User
    {
        
        private string _identifier;
        
        ///  <summary>
        /// Initializes a new instance
        /// </summary>
        public User(string identifier)
        {
            _identifier = identifier;
        }
        
        ///  <summary>
        /// The unique identifier of the user
        /// </summary>
        public string Identifier
        {
            get
            {
                return _identifier;
            }
        }
    }
    
    ///  <summary>
    /// The class ThreeDimensionalStructStructStructStructFirstDimensionSecondDimension reflects an anonymous type from the feature definition.
    /// </summary>
    [Tecan.Sila2.SilaDisplayNameAttribute("ThreeDimensionalStructStructStructStructFirstDimensionSecondDimension")]
    public struct ThreeDimensionalStructStructStructStructFirstDimensionSecondDimension
    {
        
        private string _thirdDimension;
        
        ///  <summary>
        /// Initializes a new instance
        /// </summary>
        public ThreeDimensionalStructStructStructStructFirstDimensionSecondDimension(string thirdDimension)
        {
            _thirdDimension = thirdDimension;
        }
        
        ///  <summary>
        /// Third Dimension
        /// </summary>
        public string ThirdDimension
        {
            get
            {
                return _thirdDimension;
            }
        }
    }
    
    ///  <summary>
    /// The class ThreeDimensionalStructStructStructStructFirstDimension reflects an anonymous type from the feature definition.
    /// </summary>
    [Tecan.Sila2.SilaDisplayNameAttribute("ThreeDimensionalStructStructStructStructFirstDimension")]
    public struct ThreeDimensionalStructStructStructStructFirstDimension
    {
        
        private ThreeDimensionalStructStructStructStructFirstDimensionSecondDimension _secondDimension;
        
        ///  <summary>
        /// Initializes a new instance
        /// </summary>
        public ThreeDimensionalStructStructStructStructFirstDimension(ThreeDimensionalStructStructStructStructFirstDimensionSecondDimension secondDimension)
        {
            _secondDimension = secondDimension;
        }
        
        ///  <summary>
        /// Second Dimension
        /// </summary>
        public ThreeDimensionalStructStructStructStructFirstDimensionSecondDimension SecondDimension
        {
            get
            {
                return _secondDimension;
            }
        }
    }
    
    ///  <summary>
    /// The class ThreeDimensionalStructStructStructStruct reflects an anonymous type from the feature definition.
    /// </summary>
    [Tecan.Sila2.SilaDisplayNameAttribute("ThreeDimensionalStructStructStructStruct")]
    public struct ThreeDimensionalStructStructStructStruct
    {
        
        private ThreeDimensionalStructStructStructStructFirstDimension _firstDimension;
        
        ///  <summary>
        /// Initializes a new instance
        /// </summary>
        public ThreeDimensionalStructStructStructStruct(ThreeDimensionalStructStructStructStructFirstDimension firstDimension)
        {
            _firstDimension = firstDimension;
        }
        
        ///  <summary>
        /// First Dimension
        /// </summary>
        public ThreeDimensionalStructStructStructStructFirstDimension FirstDimension
        {
            get
            {
                return _firstDimension;
            }
        }
    }
    
    ///  <summary>
    /// The class ThreeDimensionalListThreeDimensionalStructResultFirstDimensionSecondDimension reflects an anonymous type from the feature definition.
    /// </summary>
    [Tecan.Sila2.SilaDisplayNameAttribute("ThreeDimensionalListThreeDimensionalStructResultFirstDimensionSecondDimension")]
    public struct ThreeDimensionalListThreeDimensionalStructResultFirstDimensionSecondDimension
    {
        
        private string _thirdDimension;
        
        ///  <summary>
        /// Initializes a new instance
        /// </summary>
        public ThreeDimensionalListThreeDimensionalStructResultFirstDimensionSecondDimension(string thirdDimension)
        {
            _thirdDimension = thirdDimension;
        }
        
        ///  <summary>
        /// Third Dimension
        /// </summary>
        public string ThirdDimension
        {
            get
            {
                return _thirdDimension;
            }
        }
    }
    
    ///  <summary>
    /// The class ThreeDimensionalListThreeDimensionalStructResultFirstDimension reflects an anonymous type from the feature definition.
    /// </summary>
    [Tecan.Sila2.SilaDisplayNameAttribute("ThreeDimensionalListThreeDimensionalStructResultFirstDimension")]
    public struct ThreeDimensionalListThreeDimensionalStructResultFirstDimension
    {
        
        private ThreeDimensionalListThreeDimensionalStructResultFirstDimensionSecondDimension _secondDimension;
        
        ///  <summary>
        /// Initializes a new instance
        /// </summary>
        public ThreeDimensionalListThreeDimensionalStructResultFirstDimension(ThreeDimensionalListThreeDimensionalStructResultFirstDimensionSecondDimension secondDimension)
        {
            _secondDimension = secondDimension;
        }
        
        ///  <summary>
        /// Second Dimension
        /// </summary>
        public ThreeDimensionalListThreeDimensionalStructResultFirstDimensionSecondDimension SecondDimension
        {
            get
            {
                return _secondDimension;
            }
        }
    }
    
    ///  <summary>
    /// The class ThreeDimensionalListThreeDimensionalStructResult reflects an anonymous type from the feature definition.
    /// </summary>
    [Tecan.Sila2.SilaDisplayNameAttribute("ThreeDimensionalListThreeDimensionalStructResult")]
    public struct ThreeDimensionalListThreeDimensionalStructResult
    {
        
        private ThreeDimensionalListThreeDimensionalStructResultFirstDimension _firstDimension;
        
        ///  <summary>
        /// Initializes a new instance
        /// </summary>
        public ThreeDimensionalListThreeDimensionalStructResult(ThreeDimensionalListThreeDimensionalStructResultFirstDimension firstDimension)
        {
            _firstDimension = firstDimension;
        }
        
        ///  <summary>
        /// First Dimension
        /// </summary>
        public ThreeDimensionalListThreeDimensionalStructResultFirstDimension FirstDimension
        {
            get
            {
                return _firstDimension;
            }
        }
    }
    
    ///  <summary>
    /// The class WhyMakeItSimpleWhenYouCanMakeItComplicatedStructListStruct reflects an anonymous type from the feature definition.
    /// </summary>
    [Tecan.Sila2.SilaDisplayNameAttribute("WhyMakeItSimpleWhenYouCanMakeItComplicatedStructListStruct")]
    public struct WhyMakeItSimpleWhenYouCanMakeItComplicatedStructListStruct
    {
        
        private System.Collections.Generic.IDictionary<string, string> _listStruct;
        
        ///  <summary>
        /// Initializes a new instance
        /// </summary>
        public WhyMakeItSimpleWhenYouCanMakeItComplicatedStructListStruct(System.Collections.Generic.IDictionary<string, string> listStruct)
        {
            _listStruct = listStruct;
        }
        
        ///  <summary>
        /// List of Struct
        /// </summary>
        public System.Collections.Generic.IDictionary<string, string> ListStruct
        {
            get
            {
                return _listStruct;
            }
        }
    }
    
    ///  <summary>
    /// The class WhyMakeItSimpleWhenYouCanMakeItComplicatedListStructListItem reflects an anonymous type from the feature definition.
    /// </summary>
    [Tecan.Sila2.SilaDisplayNameAttribute("WhyMakeItSimpleWhenYouCanMakeItComplicatedListStructListItem")]
    public struct WhyMakeItSimpleWhenYouCanMakeItComplicatedListStructListItem
    {
        
        private System.Collections.Generic.ICollection<string> _list;
        
        ///  <summary>
        /// Initializes a new instance
        /// </summary>
        public WhyMakeItSimpleWhenYouCanMakeItComplicatedListStructListItem(System.Collections.Generic.ICollection<string> list)
        {
            _list = list;
        }
        
        ///  <summary>
        /// A list
        /// </summary>
        public System.Collections.Generic.ICollection<string> List
        {
            get
            {
                return _list;
            }
        }
    }
    
    ///  <summary>
    /// The class StructPairStringPairString reflects an anonymous type from the feature definition.
    /// </summary>
    [Tecan.Sila2.SilaDisplayNameAttribute("StructPairStringPairString")]
    public struct StructPairStringPairString
    {
        
        private string _key;
        
        private string _value;
        
        ///  <summary>
        /// Initializes a new instance
        /// </summary>
        public StructPairStringPairString(string key, string value)
        {
            _key = key;
            _value = value;
        }
        
        ///  <summary>
        /// The key
        /// </summary>
        public string Key
        {
            get
            {
                return _key;
            }
        }
        
        ///  <summary>
        /// The value
        /// </summary>
        public string Value
        {
            get
            {
                return _value;
            }
        }
    }
    
    ///  <summary>
    /// The class StructPairString reflects an anonymous type from the feature definition.
    /// </summary>
    [Tecan.Sila2.SilaDisplayNameAttribute("StructPairString")]
    public struct StructPairString
    {
        
        private StructPairStringPairString _pairString;
        
        ///  <summary>
        /// Initializes a new instance
        /// </summary>
        public StructPairString(StructPairStringPairString pairString)
        {
            _pairString = pairString;
        }
        
        ///  <summary>
        /// Pair of String
        /// </summary>
        public StructPairStringPairString PairString
        {
            get
            {
                return _pairString;
            }
        }
    }
}

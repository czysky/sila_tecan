﻿using System.Reflection;

[assembly: AssemblyTitle( "Tecan.Sila2.InteropServer" )]
[assembly: AssemblyDescription( "An interoperability server implementation using native gRPC" )]
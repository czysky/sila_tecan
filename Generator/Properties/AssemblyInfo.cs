﻿using System.Reflection;
using System.Runtime.CompilerServices;

[assembly: AssemblyTitle("SilaGen")]
[assembly: AssemblyDescription("Code generator to generate SiLA2 glue code")]
[assembly: InternalsVisibleTo( "Tecan.Sila2Generator.Test" )]
﻿namespace Tecan.Sila2
{
    /// <summary>
    /// Defines the types of schemas supported
    /// </summary>
    public enum SchemaType
    {
        /// <summary>
        /// An XML Schema
        /// </summary>
        Xml,
        /// <summary>
        /// A schema document for JSON
        /// </summary>
        Json
    }
}

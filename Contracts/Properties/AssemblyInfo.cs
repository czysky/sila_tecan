﻿using System.Reflection;

[assembly: AssemblyTitle("Tecan.Sila2.Contracts")]
[assembly: AssemblyDescription("Contracts to decouple generated code from transport layer")]
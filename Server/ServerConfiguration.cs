﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// 
// This source code was auto-generated by xsd, Version=4.8.3928.0.
// 
namespace Tecan.Sila2.Server {
    using System.Xml.Serialization;
    
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.8.3928.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true, Namespace="https://gitlab.com/SiLA2/vendors/sila_tecan/-/blob/master/Server/ServerConfigurat" +
        "ion.xsd")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace="https://gitlab.com/SiLA2/vendors/sila_tecan/-/blob/master/Server/ServerConfigurat" +
        "ion.xsd", IsNullable=false)]
    public partial class ServerConfiguration {
        
        private string identifierField;
        
        private int portField;
        
        private string nameField;
        
        private string interfaceFilterField;
        
        private bool enableServerConnectionsField;
        
        private bool enableServerConnectionsFieldSpecified;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Identifier {
            get {
                return this.identifierField;
            }
            set {
                this.identifierField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public int Port {
            get {
                return this.portField;
            }
            set {
                this.portField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string Name {
            get {
                return this.nameField;
            }
            set {
                this.nameField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public string InterfaceFilter {
            get {
                return this.interfaceFilterField;
            }
            set {
                this.interfaceFilterField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlAttributeAttribute()]
        public bool EnableServerConnections {
            get {
                return this.enableServerConnectionsField;
            }
            set {
                this.enableServerConnectionsField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool EnableServerConnectionsSpecified {
            get {
                return this.enableServerConnectionsFieldSpecified;
            }
            set {
                this.enableServerConnectionsFieldSpecified = value;
            }
        }
    }
}

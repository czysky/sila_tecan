﻿using System.Reflection;

[assembly: AssemblyTitle( "Tecan.Sila2.ParameterConstraintsProvider.Client" )]
[assembly: AssemblyDescription("Library to read constraints to parameters")]
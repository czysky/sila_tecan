//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Tecan.Sila2.Authorization.AuthorizationConfigurationService
{
    using Tecan.Sila2;
    
    
    ///  <summary>
    /// Data transfer object for the request of the Set Authorization Provider command
    /// </summary>
    [ProtoBuf.ProtoContractAttribute()]
    public class SetAuthorizationProviderRequestDto : Tecan.Sila2.ISilaTransferObject, Tecan.Sila2.ISilaRequestObject
    {
        
        private Tecan.Sila2.StringDto _authorizationProvider;
        
        ///  <summary>
        /// A regular expression to check the validity of AuthorizationProvider
        /// </summary>
        public static System.Text.RegularExpressions.Regex AuthorizationProviderPattern = new System.Text.RegularExpressions.Regex("[0-9a-f]{8}\\-[0-9a-f]{4}\\-[0-9a-f]{4}\\-[0-9a-f]{4}\\-[0-9a-f]{12}");
        
        ///  <summary>
        /// Create a new instance
        /// </summary>
        public SetAuthorizationProviderRequestDto()
        {
        }
        
        ///  <summary>
        /// Create a new instance
        /// </summary>
        /// <param name="store">An object to organize binaries.</param>
        ///  <param name="authorizationProvider">The UUID of the SiLA server that this server uses to verify access tokens.</param>
        public SetAuthorizationProviderRequestDto(string authorizationProvider, Tecan.Sila2.IBinaryStore store)
        {
            AuthorizationProvider = new Tecan.Sila2.StringDto(authorizationProvider, store);
        }
        
        ///  <summary>
        /// The UUID of the SiLA server that this server uses to verify access tokens.
        /// </summary>
        [ProtoBuf.ProtoMemberAttribute(1)]
        public Tecan.Sila2.StringDto AuthorizationProvider
        {
            get
            {
                return _authorizationProvider;
            }
            set
            {
                _authorizationProvider = value;
            }
        }
        
        ///  <summary>
        /// Gets the command identifier for this command
        /// </summary>
        /// <returns>The fully qualified command identifier</returns>
        public string CommandIdentifier
        {
            get
            {
                return "org.silastandard/core/AuthorizationConfigurationService/v1/Command/SetAuthorizati" +
                    "onProvider";
            }
        }
        
        ///  <summary>
        /// Validates the contents of this transfer object
        /// </summary>
        /// <returns>A validation error or null, if no validation error occurred.</returns>
        public string GetValidationErrors()
        {
            string errors = "";
            if (AuthorizationProviderPattern.IsMatch(AuthorizationProvider.Value))
            {
                errors = (errors + string.Format("AuthorizationProvider \'{0}\' does not match pattern \'[0-9a-f]{8}\\-[0-9a-f]{4}\\-[0-" +
                        "9a-f]{4}\\-[0-9a-f]{4}\\-[0-9a-f]{12}\'", AuthorizationProvider.Value));
            }
            return errors;
        }
    }
    
    ///  <summary>
    /// Data transfer object to encapsulate the response of the Authorization Provider property
    /// </summary>
    [ProtoBuf.ProtoContractAttribute()]
    public class AuthorizationProviderResponseDto : Tecan.Sila2.ISilaTransferObject
    {
        
        private Tecan.Sila2.StringDto _value;
        
        ///  <summary>
        /// A regular expression to check the validity of Value
        /// </summary>
        public static System.Text.RegularExpressions.Regex ValuePattern = new System.Text.RegularExpressions.Regex("[0-9a-f]{8}\\-[0-9a-f]{4}\\-[0-9a-f]{4}\\-[0-9a-f]{4}\\-[0-9a-f]{12}");
        
        ///  <summary>
        /// Create a new instance
        /// </summary>
        public AuthorizationProviderResponseDto()
        {
        }
        
        ///  <summary>
        /// Create a new instance
        /// </summary>
        /// <param name="store">An object to organize binaries.</param>
        ///  <param name="value"></param>
        public AuthorizationProviderResponseDto(string value, Tecan.Sila2.IBinaryStore store)
        {
            Value = new Tecan.Sila2.StringDto(value, store);
        }
        
        ///  <summary>
        /// The  property
        /// </summary>
        [ProtoBuf.ProtoMemberAttribute(1)]
        public Tecan.Sila2.StringDto Value
        {
            get
            {
                return _value;
            }
            set
            {
                _value = value;
            }
        }
        
        ///  <summary>
        /// Validates the contents of this transfer object
        /// </summary>
        /// <returns>A validation error or null, if no validation error occurred.</returns>
        public string GetValidationErrors()
        {
            string errors = "";
            if (ValuePattern.IsMatch(Value.Value))
            {
                errors = (errors + string.Format("Value \'{0}\' does not match pattern \'[0-9a-f]{8}\\-[0-9a-f]{4}\\-[0-9a-f]{4}\\-[0-9a-" +
                        "f]{4}\\-[0-9a-f]{12}\'", Value.Value));
            }
            return errors;
        }
    }
}
